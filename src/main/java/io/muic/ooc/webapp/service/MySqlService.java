package io.muic.ooc.webapp.service;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by Bank on 2/16/17.
 */
public class MySqlService {
    enum TestTableColumns {
        id,firstname,lastname,username,password;
    }

    private final String jdbcDriverStr;
    private final String jdbcURL;

    private Connection connection;
    private Statement statement;
    private ResultSet resultSet;
    private PreparedStatement preparedStatement;

    public MySqlService(String jdbcDriverStr, String jdbcURL) {
        this.jdbcDriverStr = jdbcDriverStr;
        this.jdbcURL = jdbcURL;
    }

    public ArrayList<String[]> readData() throws Exception {
        ArrayList<String[]> rs = new ArrayList<>();
        try {
            Class.forName(jdbcDriverStr);
            connection = DriverManager.getConnection(jdbcURL + "?useSSL=false", "root", "goodbank1");
            statement = connection.createStatement();

            resultSet = statement.executeQuery("select * from bank;");
            rs = getResultSet(resultSet);

        }catch (Exception e){
            e.printStackTrace();
        }
        finally {
            close();
            return rs;
        }
    }

    public void writeData(String firstname, String lastname,String username, String password) {
        try {
            Class.forName(jdbcDriverStr);

            connection = DriverManager.getConnection(jdbcURL + "?useSSL=false", "root", "goodbank1");

            preparedStatement = connection.prepareStatement("insert into bank (firstname,lastname,username,password) values (?,?,?,?);");
            preparedStatement.setString(1,firstname);
            preparedStatement.setString(2,lastname);
            preparedStatement.setString(3,username);
            preparedStatement.setString(4, PasswordEncrypt.getSecurePassword(username,password));
            preparedStatement.executeUpdate();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private ArrayList<String[]> getResultSet(ResultSet resultSet) throws Exception {
        ArrayList<String[]> result = new ArrayList<>();
        while (resultSet.next()) {
            String id = String.valueOf(resultSet.getInt(String.valueOf(TestTableColumns.id)));
            String fn = resultSet.getString(TestTableColumns.firstname.toString());
            String ln = resultSet.getString(TestTableColumns.lastname.toString());
            String user = resultSet.getString(TestTableColumns.username.toString());
            String pass = resultSet.getString(TestTableColumns.password.toString());
            String[] temp = {id,fn,ln,user,pass};
            result.add(temp);
        }
        return result;
    }

    private void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (Exception ignored) {
        }
    }

    public boolean isInDatabase(String user) throws Exception {
        readData();
        for (String[] x :readData()){
            if (x[3].equals(user)){
                System.out.println(x[3].equals(user));
                return true;
            }
        }
        return false;
    }

    public void deleteUser(int userId){
        try {
            Class.forName(jdbcDriverStr);

            connection = DriverManager.getConnection(jdbcURL + "?useSSL=false", "root", "goodbank1");

            preparedStatement = connection.prepareStatement("delete from bank where id = "+userId);
            preparedStatement.executeUpdate();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void updateUser(String firstname,String lastname,int userId){
        try {
            Class.forName(jdbcDriverStr);
            connection = DriverManager.getConnection(jdbcURL + "?useSSL=false", "root", "goodbank1");
            if (firstname.isEmpty()){
                preparedStatement = connection.prepareStatement("update bank set lastname = '"+lastname+"' where id = "+userId);
                preparedStatement.executeUpdate();
            }

            if (lastname.isEmpty()){
                preparedStatement = connection.prepareStatement("update bank set firstname = '"+firstname+"' where id = "+userId);
                preparedStatement.executeUpdate();
            }

            if (!firstname.isEmpty()&&!lastname.isEmpty()){
                preparedStatement = connection.prepareStatement("update bank set firstname = '"+firstname+"',"+"lastname = '"+lastname+"' where id = "+userId);
                preparedStatement.executeUpdate();
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }


}