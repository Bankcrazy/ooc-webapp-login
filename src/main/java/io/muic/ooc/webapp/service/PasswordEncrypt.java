package io.muic.ooc.webapp.service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Bank on 2/17/17.
 */
public class PasswordEncrypt {

    public static String getSecurePassword(String username, String password)
    {
        String SALT = username + password;
        StringBuilder hash = new StringBuilder();
        try {
            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            byte[] hashedBytes = sha.digest(SALT.getBytes());
            char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                    'a', 'b', 'c', 'd', 'e', 'f' };
            for (int idx = 0; idx < hashedBytes.length; ++idx) {
                byte b = hashedBytes[idx];
                hash.append(digits[(b & 0xf0) >> 4]);
                hash.append(digits[b & 0x0f]);
            }
        } catch (NoSuchAlgorithmException e) {

        }
        return hash.toString();
    }
}