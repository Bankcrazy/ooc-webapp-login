/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.muic.ooc.webapp.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author gigadot
 */
public class SecurityService {
    private MySqlService mySqlService = new MySqlService("com.mysql.jdbc.Driver","jdbc:mysql://localhost:3306/sys");

    public boolean isAuthorized(HttpServletRequest request) {
        String username = (String) request.getSession().getAttribute("username");
        try {
            if (mySqlService.isInDatabase(username)) {
                return (username != null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean authenticate(String username, String password) throws Exception {
        ArrayList<String[]> result =  mySqlService.readData();
        String securePassword = PasswordEncrypt.getSecurePassword(username,password);
        for(String[] each : result) {
            if (each[3].equals(username)&&each[4].equals(securePassword)){
                return true;
            }
        }
        return false;
    }
}
