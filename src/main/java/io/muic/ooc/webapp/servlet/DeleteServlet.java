package io.muic.ooc.webapp.servlet;

import io.muic.ooc.webapp.service.MySqlService;
import io.muic.ooc.webapp.service.SecurityService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Bank on 2/17/17.
 */
public class DeleteServlet extends HttpServlet {

    private String target;
    private int targetID;
    private SecurityService securityService = new SecurityService();
    private MySqlService mySqlService = new MySqlService("com.mysql.jdbc.Driver","jdbc:mysql://localhost:3306/sys");
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean authorized = securityService.isAuthorized(request);
        if (authorized) {
            targetID = Integer.parseInt(request.getParameter("userID"));
            target = request.getParameter("selectedUser");
            RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/delete.jsp");
            rd.include(request, response);
        } else {
            response.sendRedirect("/login");
        }
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String currentUser = (String) request.getSession().getAttribute("username");
        if(currentUser.equals(target)){
            String error = "Cannot remove/delete yourself";
            request.setAttribute("error", error);
            RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/delete.jsp");
            rd.include(request, response);
        } else {
            mySqlService.deleteUser(targetID);
            response.sendRedirect("/");
        }
    }
}

