package io.muic.ooc.webapp.servlet;

import io.muic.ooc.webapp.service.MySqlService;
import io.muic.ooc.webapp.service.SecurityService;
import org.apache.commons.lang.StringUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Bank on 2/17/17.
 */
public class AddUserServlet extends HttpServlet {
    private MySqlService mySqlService = new MySqlService("com.mysql.jdbc.Driver","jdbc:mysql://localhost:3306/sys");
    private SecurityService securityService = new SecurityService();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean authorized = securityService.isAuthorized(request);
        if (authorized) {
            RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/add.jsp");
            rd.include(request, response);
        } else {
            response.sendRedirect("/login");

        }

    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String firstname = request.getParameter("first name");
        String lastname = request.getParameter("last name");
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        if (StringUtils.isBlank(username) || StringUtils.isBlank(password) || StringUtils.isBlank(firstname) || StringUtils.isBlank(lastname)) {
            String error = "There are missing required field";
            request.setAttribute("error", error);
            RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/add.jsp");
            rd.include(request, response);
        } else {
            try {
                if (!mySqlService.isInDatabase(username)) {
                    mySqlService.writeData(firstname,lastname,username, password);
                    response.sendRedirect("/");
                } else {
                    String error = "Username already exists";
                    request.setAttribute("error", error);
                    RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/add.jsp");
                    rd.include(request, response);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
