/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.muic.ooc.webapp.servlet;

import io.muic.ooc.webapp.service.MySqlService;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author gigadot
 */
public class RegisterServlet extends HttpServlet {
     private MySqlService mySqlService = new MySqlService("com.mysql.jdbc.Driver","jdbc:mysql://localhost:3306/sys");

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/register.jsp");
        rd.include(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String firstname = request.getParameter("first name");
        String lastname = request.getParameter("last name");
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        if (StringUtils.isBlank(username) || StringUtils.isBlank(password) || StringUtils.isBlank(firstname) || StringUtils.isBlank(lastname)) {
            String error = "There are missing required field";
            request.setAttribute("error", error);
            RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/register.jsp");
            rd.include(request, response);
        } else {
            try {
                if (!mySqlService.isInDatabase(username)) {
                    mySqlService.writeData(firstname,lastname,username, password);
                    response.sendRedirect("/login");
                } else {
                    String error = "Username already exists";
                    request.setAttribute("error", error);
                    RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/register.jsp");
                    rd.include(request, response);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

