/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.muic.ooc.webapp.servlet;

import io.muic.ooc.webapp.service.MySqlService;
import io.muic.ooc.webapp.service.SecurityService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author gigadot
 */
public class UserServlet extends HttpServlet {
    private SecurityService securityService = new SecurityService();
    private MySqlService mySqlService = new MySqlService("com.mysql.jdbc.Driver","jdbc:mysql://localhost:3306/sys");

    private String[] findInfo(String username) throws Exception {
        ArrayList<String[]> result =  mySqlService.readData();
        String answer[] = {"","","","",""};
        for(String[] each : result) {
            if (each[3].equals(username)){
                answer[0] = each[0];
                answer[1] = each[1];
                answer[2] = each[2];
                answer[3] = each[3];
                answer[4] = each[4];
                return answer;
            }
        }
        return answer;
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean authorized = securityService.isAuthorized(request);
        if (authorized) {
            String currentUser = (String) request.getSession().getAttribute("username");
            try {
                String result[] = findInfo(currentUser);
                String id = result[0];
                String fn = result[1];
                String ln = result[2];
                String user = result[3];
                String pass =  (String) request.getSession().getAttribute("password");
                request.setAttribute("id", id);
                request.setAttribute("fn", fn);
                request.setAttribute("ln", ln);
                request.setAttribute("user", user);
                request.setAttribute("pass", pass);
                RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/user.jsp");
                rd.include(request, response);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            response.sendRedirect("/login");
        }

    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("/");
    }
}
