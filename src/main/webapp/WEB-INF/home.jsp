<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>

<html>
    <body>
    <h2>User List Page</h2>
    </body>

    <form action="/user" method="get">
        <br>
        <input type="submit" value="My Info">
    </form>

    <form method="get">

    <table border="2">
        <tr>
            <td>ID</td>
            <td>FIRSTNAME</td>
            <td>LASTNAME</td>
            <td>USERNAME</td>
            <td>PASSWORD</td>
            <td>ACTION</td>
        </tr>

        <% try {
            Class.forName("com.mysql.jdbc.Driver");
            String url="jdbc:mysql://localhost:3306/sys";
            String username="root";
            String password="goodbank1";
            String query="SELECT * FROM sys.bank;";
            Connection conn=DriverManager.getConnection(url,username,password);
            Statement stmt=conn.createStatement();
            ResultSet rs=stmt.executeQuery(query);
            while(rs.next()) {
        %>
        <tr>
            <% String s = rs.getString("Username"); %>
            <% int i = rs.getInt("ID"); %>

            <td><%=rs.getInt("ID") %></td>
            <td><%=rs.getString("Firstname") %></td>
            <td><%=rs.getString("Lastname") %></td>
            <td><%=rs.getString("Username") %></td>
            <td><%=rs.getString("Password") %></td>

            <td>
                <form action="/edit?" method="get">
                    <input type="hidden" name="userID"  value="<%=i%>">
                    <input type="submit" value="Edit">
                </form>

                <form action="delete?" method="get">
                    <input type="hidden" name="selectedUser"  value="<%=s%>">
                    <input type="hidden" name="userID"  value="<%=i%>">
                    <input type="submit" value="Delete">

                </form>
            </td>
        </tr>
        <%}%>
    </table>

    <%
        rs.close();
        stmt.close();
        conn.close();
        } catch(Exception e){
        e.printStackTrace();
        }
    %>
    <br>
    <form action="/add" method="get">
        <br>
        <input type="submit" value="Add User">
    </form>

    <form action="/" method="post">
        <br>
        <input type="submit" value="Logout">
    </form>

</html>