<html>
    <body>
        <h2>Delete User Confirmation</h2>
        <p>Are you sure you want to delete/remove the user</p>
        <br>
        <p>${error}</p>
        <form action="/" method="get">
            <br>
            <input type="submit" value="Cancel">
        </form>
        <form action="/delete" method="post">
            <br>
            <input type="submit" value="Confirm">
        </form>
    </body>
</html>